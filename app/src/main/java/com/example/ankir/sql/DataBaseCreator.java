package com.example.ankir.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import static com.example.ankir.sql.DataBaseCreator.User.TABLE_NAME;

/**
 * Created by ankir on 10.06.2017.
 */


    public class DataBaseCreator extends SQLiteOpenHelper {
        public static final String DB_NAME = "db_name";
        public static final int DB_VERSION = 2;


        public static class User implements BaseColumns {
            public static final String TABLE_NAME = "t_main";
            public static final String MAIN_USER = "user_name";
            public static final String USER_AGE = "user_age";

        }

        static String SCRIPT_CREATE_TBL_MAIN = "CREATE TABLE " + TABLE_NAME + " (" +
                User._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                User.MAIN_USER + " TEXT, " +
                User.USER_AGE + " INTEGER" +
                ");";

        public DataBaseCreator(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SCRIPT_CREATE_TBL_MAIN);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //update, if need
            //1 через удаление базы
          //  db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
          //  onCreate(db);

            //2 через добавление
             db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD " + User.USER_AGE + " INTEGER");
        }
    }


