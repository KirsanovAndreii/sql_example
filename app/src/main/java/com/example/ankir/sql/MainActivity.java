package com.example.ankir.sql;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    DataBaseMaster dbMaster;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbMaster = DataBaseMaster.getInstance(this);
       for (int i=0;i<20;i++)
       {
           dbMaster.insertUser(new User("Vas"+i, i+20));
       }

        List <User> users = dbMaster.getUsers();

        for (User user : users) {
            Log.d("User", user.toString());
        }

    }
}
