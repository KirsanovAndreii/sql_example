package com.example.ankir.sql;

/**
 * Created by ankir on 10.06.2017.
 */

public class User {
    String name;
    int age;

    public User() {
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User("+"Name " + name + "/ age "+ age+")";
    }
}
